<?php

namespace App\DataFixtures;

use App\Entity\Groupe;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GroupeFixtures extends Fixture  implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        $groupes = ['Soutien Alcohol', 'La procrastination', 'Se mettre au sport !'];

        for ($i = 0; $i < sizeof($groupes); $i++) {
            $groupe = (new Groupe())
                ->setSubject($groupes[$i]);

            $manager->persist($groupe);
        }

        $manager->flush();
    }


    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(UserFixtures::class);
    }
}
