<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        $users = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < 80; $i++) {
            $post = (new Post())
                ->setTitle($faker->title)
                ->setContent($faker->sentence(20))
                ->setUserId($faker->randomElement($users))
                ->setDate($faker->dateTime)
            ;

            $manager->persist($post);
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(UserFixtures::class);
    }
}
