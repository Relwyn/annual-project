<?php

namespace App\Repository;

use App\Entity\Message;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function getMessages(int $receiverId, int $senderId, DateTime $date = null){
        dump($date);
        $qb = $this->createQueryBuilder('m');
        $qb->select(array('m'))
            ->where($qb->expr()->andX(
                $qb->expr()->eq('m.senderId', ':senderId'),
                $qb->expr()->eq('m.receiverId', ':receiverId')
            ))
            ->orWhere($qb->expr()->andX(
                $qb->expr()->eq('m.receiverId', ':senderId'),
                $qb->expr()->eq('m.senderId', ':receiverId')
            ));

        if(isset($date)){
            $qb->andWhere( 'm.createdAt > :date' );
            $qb->setParameter('date',$date->format('Y-m-d H:i:s'));
        }
        $qb->setParameter('receiverId', $receiverId)
            ->setParameter('senderId', $senderId)
            ->orderBy('m.createdAt', 'ASC');
        $query = $qb->getQuery();
        if(isset($date)) {
            $query->setHint(Query::HINT_INCLUDE_META_COLUMNS, true);
            dump($query->getArrayResult());
            return $query->getArrayResult();
        } else{
            return $query->getResult();
        }
    }

    public function getMessagesGroupe( int $groupeId, DateTime $date = null){
        $qb = $this->createQueryBuilder('m');
        $qb->select(array('m'))
        ->where($qb->expr()->andX(
            $qb->expr()->eq('m.groupeId', ':groupeId')
        ));

        if(isset($date)){
            $qb->andWhere( 'm.createdAt > :date' );
            $qb->setParameter('date',$date->format('Y-m-d H:i:s'));
        }
        $qb->setParameter('groupeId', $groupeId)
            ->orderBy('m.createdAt', 'ASC');
        $query = $qb->getQuery();
        if(isset($date)) {
            $query->setHint(Query::HINT_INCLUDE_META_COLUMNS, true);
            return $query->getArrayResult();
        } else{
            return $query->getResult();
        }
    }

}
