<?php

namespace App\Controller\Front;

use App\Entity\Comment;
use App\Entity\Groupe;
use App\Entity\Post;
use App\Entity\Tag;
use App\Entity\User;
use App\Entity\Event;
use App\Form\CommentType;
use App\Form\PostType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\GroupeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user", name="user_")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('front/user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET","POST"})
     */
    public function show(Request $request,MessageBusInterface $bus, User $user): Response
    {
        $comment = new Comment();
        $post = new Post();
        $posts = $user->getPosts();
        $requestData = $request->request->all();

        if (isset($requestData["postId"])) {
            $form = $this->createForm(CommentType::class, $comment);
            $form->handleRequest($request);
            // Check if the form is valid
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $post = $this->getDoctrine()->getRepository(Post::class)->find($requestData["postId"]);
                $comment->setPostId($post);
                $comment->setUserId($user);
                $entityManager->persist($comment);
                $entityManager->flush();

                return $this->redirectToRoute('post_show', [
                    'id' => $post->getId(),
                ]);
            }
        } else {
            $form = $this->createForm(PostType::class, $post);
            $form->handleRequest($request);
            // Check if the form is valid
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                // Get tags and save them
                $postContent = $post->getContent();
                preg_match_all("~#[a-z]*( |$)~", $postContent, $tags);
                $tags = $tags[0];

                for($j = 0; $j < sizeof($tags); $j++){
                    $tagName = $tags[$j];
                    $tagName = substr($tagName, 1);
                    $tag = new Tag();
                    $tag->setName($tagName);
                    $tag->addPostId($post);
                    $entityManager->persist($tag);
                    $post->addTag($tag);
                }

                $post->setUserId($user);
                $entityManager->persist($post);
                $entityManager->flush();

                // Send notification to friends
                $friends = $user->getFriends();
                foreach ($friends as $friend) {
                    $id = $friend->getId();
                    dump("post/newPost/".$id);
                    $update = new Update("post/newPost/".$id,
                        json_encode(['message' => $user->getSlug().' a créer un nouveau post !'])
                    );
                    $bus->dispatch($update);
                }

                //return $this->redirectToRoute('default_index');
            }
        }
//        $posts = $this->getDoctrine()->getRepository(Post::class)->findTenPost();

        // create postFrom
        $postForm = $this->createForm(PostType::class, $post);
        // create comment Form
        $data = [];
        for ($i = 0; $i < sizeof($posts); $i++) {
            $data[$i] = [];
            $data[$i]["post"] = $posts[$i];
            $form = $this->createForm(CommentType::class, $comment);
            $data[$i]["form"] = $form->createView();
        }

        return $this->render('front/user/show.html.twig', [
            'data' => $data,
            'postForm' => $postForm->createView(),
            'user' => $user,
            'posts' => $posts,
        ]);

//        'user' => $user,
//            'data' => $data,
//
//            'postForm' => $postForm->createView()
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

//            return $this->redirectToRoute('user_index');
        }

        return $this->render('front/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/addFriend/{id}", name="addFriend", methods={"GET"})
     */
    public function addFriend(User $newFriend): Response
    {
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();
        $user->addFriend($newFriend);
        $newFriend->addFriend($user);

        $entityManager->persist($user);
        $entityManager->flush();

        $this->addFlash('sucess', "Vous êtes maintenant ami avec ". $newFriend->getLastname(). " !");

        return $this->redirectToRoute('message_index');
    }

    /**
     * @Route("/deleteFriend/{id}", name="friend_delete", methods={"GET","POST"})
     */
    public function deleteFriend(Request $request, User $receiver): Response
    {
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();
        $user->removeFriend($receiver);
        $receiver->removeFriend($user);
        $entityManager->flush();

        $this->addFlash('sucess', "Vous n'êtes plus ami avec " . $receiver->getLastname()."!");

        return $this->redirectToRoute('message_index');
    }

    /**
     * @Route("/joinGroup/{id}", name="join_group", methods={"GET"})
     */
    public function joinGroup(Groupe $groupe): Response
    {
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();
        $groupeService = new GroupeService($entityManager);

        $groupeService->joinGroup($user, $groupe);

        $this->addFlash('sucess', "Vous avez rejoint le groupe ". $groupe->getSubject(). " !");

        return $this->redirectToRoute('message_discussion_group', ['id' => $groupe->getId()]);
    }

    /**
     * @Route("/leaveGroup/{id}", name="leave_groupe", methods={"GET","POST"})
     */
    public function leaveGroupe(Request $request, Groupe $groupe): Response
    {
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();
        $groupeService = new GroupeService($entityManager);

        $groupeService->leaveGroupe($user, $groupe);

        $this->addFlash('sucess', "Vous avez quitté le groupe " . $groupe->getSubject()." !");

        return $this->redirectToRoute('message_index');
    }

}
