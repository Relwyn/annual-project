<?php

namespace App\Tests\Repository;

use App\Entity\Groupe;
use App\Entity\User;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserUnitTest extends KernelTestCase

{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearchByEmail()
    {
        $user = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['email' => 'test@test.com'])
        ;

        $this->assertNotNull($user);
        $this->assertSame('test', $user->getLastname());
        $this->assertSame('tester', $user->getFirstName());
    }
    public function testCreateUser()
    {
        $faker = \Faker\Factory::create();

        $user = (new User())
            ->setLastname($faker->lastName)
            ->setFirstname($faker->firstName)
            ->setEmail($faker->email)
            ->setPlainPassword('string')
            ->setGender($faker->randomElement(User::GENDER))
            ->setBirthday(new DateTime($faker->date($format = 'Y-m-d', $max = 'now')));


        // Default groupe
        $groupe = (new Groupe());
        $groupe->setSubject('Les nouveaux !');
        $user->addGroupe($groupe);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->assertNotNull($user);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
