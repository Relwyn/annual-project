/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../scss/messages.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');

console.log('Hello Webpack Encore! Edit me in assets/js/messages.js');

Date.prototype.yyyymmdd = function () {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
    (mm > 9 ? '' : '0') + mm,
    (dd > 9 ? '' : '0') + dd
    ].join('');
};

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

let dates = document.querySelectorAll('.date');

var customDate = new Date();
customDate = customDate.getMonth() + "/" + customDate.getDay() + "/" + customDate.getFullYear() + " " + customDate.getHours() + ":" + customDate.getMinutes();

// console.log("customDate", customDate);
let lastDate = dates[0] ? dates[dates.length - 1].dataset.fulldate : customDate;
// console.log("lastDate", lastDate);
let userId = document.getElementById('userId').value;
let userOrGroupe = document.getElementById('receiverId') ? "receiver" : "groupe";
let receiverId = userOrGroupe === "receiver" ? document.getElementById('receiverId').value : document.getElementById('groupeId').value;

let textarea = document.getElementById('message_content');
let sendButton = document.getElementById('message_Envoyer');
let form = document.getElementsByTagName('form')[0];
let messagesContainer = document.getElementById('messages');
messagesContainer.scrollBy(0, 100000);

let firstMessage = document.getElementById('firstMessage');

let currentUrl = window.location.href;
let url = userOrGroupe === "receiver" ? currentUrl.replace('discussion', 'get') : currentUrl.replace('discussion', 'get');

textarea.addEventListener('keyup', function (event) {
    if (event.keyCode === 13 && event.altKey === false) {
        event.preventDefault();
        sendButton.click();
    }
});
sendButton.addEventListener('click', function (event) {
    event.preventDefault();
    console.log("message sended");
    let xhttp = new XMLHttpRequest();
    xhttp.open("POST", window.location.href.replace('discussion', 'new'), true);
    xhttp.send(new FormData(form));
    textarea.value = "";
    xhttp.onreadystatechange = function (json) {
        if (this.readyState === 4 && this.status === 200) {
            // let responseText = JSON.parse(this.responseText);
            // let content = JSON.parse(responseText.content);
            // let contentJson =  JSON.parse(content);
        }
    }
});

getMessage();

function getMessage() {
    console.log("getMessages");
    let xhttp = new XMLHttpRequest();
    let params = "lastDate=" + lastDate;
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.send(params);
    xhttp.onreadystatechange = function (json) {
        if (this.readyState === 4 && this.status === 200) {

            let responseText = JSON.parse(this.responseText);
            window.response = responseText;
            if (responseText.empty) {
                console.log("no new messages");
                setTimeout(getMessage, 2000);

                return true;
            }

            let messages = responseText;
            console.log("messages", messages);
            messages.forEach((message, index) => {
                if (!isEmpty(message)) {
                    console.log("not empty");
                    console.log(message);
                    let currentUser
                    if (userOrGroupe === "receiver") {
                        currentUser = receiverId == message.receiver_id_id ? `sender` : 'receiver';
                    } else {
                        currentUser = receiverId == message.groupe_id_id ? `sender` : 'receiver';
                    }
                    let messageHtml = `
                        <div class="message">
                            <img class="profile-pic" src="${message.senderId.imageFileName}" alt="Avatar">
                            <div class="line">
                                <div class="upperLine">
                                    <p class="userName"> ${message.senderId.firstname}</p> <p class="date" data-fulldate="${message.createdAt.date}">${message.createdAt.date.split(' ')[1].split('.')[0]}</p>
                                </div>
                                <div class="content">${message.content}</div>
                            </div>
                        </div>
                    `;

                    // let messageHtml = `<div class"message` + currentUser + `">`
                    //     + `<p class="userName">` + message.senderId.firstname + `</p>`
                    //     + `<div class="line">`
                    //     + `<div class="content">` + message.content + `</div>`
                    //     + `<div class="date" data-fulldate="` + message.createdAt.date + `">` + message.createdAt.date.split(' ')[1].split('.')[0] + `</div>`
                    //     + `</div>`
                    //     + `</div>`;
                    // console.log("messageHtml", messageHtml);
                    let messageNode = document.createElement('div');
                    messageNode.classList.add('row');
                    messageNode.classList.add(currentUser);
                    messageNode.innerHTML = messageHtml;

                    messagesContainer.appendChild(messageNode);
                }
            });

            dates = document.querySelectorAll('.date');

            var customDate = new Date();
            customDate = customDate.getMonth() + "/" + customDate.getDay() + "/" + customDate.getFullYear() + " " + customDate.getHours() + ":" + customDate.getMinutes();

            lastDate = dates[0] ? dates[dates.length - 1].dataset.fulldate : customDate;
            console.log("lastDate", lastDate);

            if (firstMessage) firstMessage.remove();
            messagesContainer.scrollBy(0, 100000);
            getMessage();

        } else if (this.status === 500) {
            console.log("500:", this);
        }
    }
}




