# PropUp

Welcome to PropUp the social media for people that want change in their live !

To run this project we highly recommand the use of Docker.

Some of the commands below are regrouped in the [makefile](./Makefile) to simplify the use.

## Dependencies

 - docker-compose 1.18
 - docker 19.03
 - yarn
 
### Dependencies without docker
 - symfony 5
 - php 7.4
 - postgres 12
 - composer 1.10
 - yarn 1.22

## Install dependencies

Change your .env file into .env.[env] and enter the credentials for postgres and run the following:

```
    # Run docker
    docker-compose up
    
    # Install composer dependencies
    docker-compose exec php composer install
    
    # Install yarn dependencies
    
    yarn install``
    
    # Compile assets
    
    yarn watch
``` 

Open your browser on : http://localhost:7080/  

### Import fixtures

If you need data to work you need to run :

```
    # Create database
    docker-compose exec php bin/console d:d:c

    # Update scheme
    docker-compose exec php bin/console d:s:u --force

    # Import firxtures
    docker-compose exec php bin/console d:f:l
```

You can now log with :
 - User account : 
    * mail    : *arnaud@gmail.com*  
    * password              : *string*
 - Admin account : 
    * mail    : *admin@gmail.com*  
    * password             : *string*

## Testing

All test are in "tests" directory

### End-to-end test

Before running the test you need to [import fixtures](#import-fixtures)

Then you need to change in .env file your APP_ENV variable into "test" (APP_ENV=test)

To run all test use 

```
   yarn run test
```

To run one test file, for example inscriptionSpect.test.js, use :
```
    yarn run test post
```

### Unit and functionnal test

To run all unit and functionnal test use

```
    docker-compose exec php bin/phpunit tests/functionnalTest

    docker-compose exec php bin/phpunit tests/unitTest
```

### Third party testing

-  [Blackfire](./documentation/BLACKFIRE.md)
-  [Vegeta](./documentation/VEGETA.md)


## Symfony list of features

Below all symfony features used in this project and link to documentation  
    
- [Symfony features](./documentation/SYMFONY_FEATURES.md)
