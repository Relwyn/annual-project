<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        $users = $manager->getRepository(User::class)->findAll();
        $posts = $manager->getRepository(Post::class)->findAll();

        try {
            for ($i = 0; $i < 100; $i++) {
                $comment = (new Comment())
                    ->setContent($faker->sentence(20))
                    ->setUserId($faker->randomElement($users))
                    ->setPostId($faker->randomElement($posts));

                $manager->persist($comment);
            }
        } catch (\Exception $e) {
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            PostFixtures::class,
            );
    }
}
