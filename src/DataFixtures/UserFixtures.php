<?php

namespace App\DataFixtures;

use App\Entity\Groupe;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFixtures extends Fixture  implements FixtureGroupInterface, ContainerAwareInterface
{
    private $passwordEncoder;
    private $container;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        // Default user
        $user = (new User())
            ->setId(0)
            ->setLastname('Arnaud')
            ->setFirstname('Luviac')
            ->setEmail('arnaud@gmail.com')
            ->setPlainPassword('string')
            ->setImageFilename('/build/images/users/zorian.png')
            ->setBackgroundImageFileName('/build/images/background/plage.jpg')
            ->setGender("m")
            ->setBirthday(new DateTime($faker->date($format = 'Y-m-d', $max = 'now')));
        ;

        // Default groupe
        $groupe = (new Groupe());
        $groupe->setSubject('Les nouveaux !');
        $user->addGroupe($groupe);

        $manager->persist($user);

        // Default Admin
        $admin = (new User())
            ->setId(2)
            ->setLastname('Admin')
            ->setFirstname('SuperUser')
            ->setEmail('admin@gmail.com')
            ->setPlainPassword('string')
            ->setImageFilename('/build/images/users/zorian.png')
            ->setBackgroundImageFileName('/build/images/background/plage.jpg')
            ->setRoles(['ROLE_ADMIN'])
            ->setGender($faker->randomElement(User::GENDER))
            ->setBirthday(new DateTime($faker->date($format = 'Y-m-d', $max = 'now')));
        ;

        $admin->addGroupe($groupe);
        $manager->persist($admin);

        // Create images directory and add User
        $images = scandir('./assets/images/users/');
        $imageDirectory =  $this->container->getParameter('images_directory') . "/users/";
        $imageDirectory = explode('/public', $imageDirectory)[1];

        array_shift($images);
        array_shift($images);

        for($j =0; $j < sizeof($images); $j++){
            $images[$j] = $imageDirectory.$images[$j];
        }

        for ($i = 0; $i < sizeof($images)-1; $i++) {
            $user = (new User())
                ->setLastname($faker->lastName)
                ->setFirstname($faker->firstName)
                ->setEmail($faker->email)
                ->setPlainPassword('string')
                ->setImageFilename($images[$i])
                ->setBackgroundImageFileName('/build/images/background/plage.jpg')
                ->setGender($faker->randomElement(User::GENDER))
                ->setBirthday(new DateTime($faker->date($format = 'Y-m-d', $max = 'now')));

            $manager->persist($user);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['fixtures'];
    }
}
