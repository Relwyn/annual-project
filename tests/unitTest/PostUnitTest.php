<?php

namespace App\Tests\Repository;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\Tag;
use App\Entity\User;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PostUnitTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    private $post;
    private $comment;
    private $tag;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->post = ( new Post())
            ->setContent('je suis du contenue');

        $this->comment = ( new Comment())
            ->setContent('je suis un commentaire');

        $this->tag = (new Tag())
            ->setLabel('je suis un tag');

    }

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearchByTitle()
    {
        $user = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['email' => 'test@test.com'])
        ;

        $post = $this->entityManager
            ->getRepository(Post::class)
            ->findOneBy(['title' => 'Post test'])
        ;

        $this->assertNotNull($post);
        $this->assertSame($user->getId(), $post->getUserId()->getId());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testFindTenPost()
    {
        $result = $this->entityManager->getRepository(Post::class)->findTenPost();
        $this->assertEquals(10, sizeof($result));
    }

    public function testIsValidContent()
    {
        $this->assertEquals(false, $this->post->isValidContent() );
    }

    public function testIsValidComments()
    {
        $this->assertEquals(false, $this->post->isValidComments() );
    }

    public function testAddComment()
    {
        $this->post->addComment($this->comment);

        $this->assertIsString( $this->comment->getContent());
        $this->assertEquals( 'je suis un commentaire', $this->comment->getContent());
    }

    public function testAddTag()
    {
        $this->post->addTag($this->tag);

        $this->assertIsString( $this->tag->getLabel());
        $this->assertEquals( 'je suis un tag', $this->tag->getLabel());
    }

}
