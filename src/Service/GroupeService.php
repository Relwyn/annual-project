<?php

namespace App\Service;


use App\Entity\Groupe;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class GroupeService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    public function joinGroup(User $user, Groupe $groupe)
    {

        $groupe->addUser($user);

        $this->entityManager->persist($groupe);
        $this->entityManager->flush();
    }

    public function leaveGroupe(User $user, Groupe $groupe)
    {

        $groupe->removeUser($user);

        $this->entityManager->persist($groupe);
        $this->entityManager->flush();
    }
}