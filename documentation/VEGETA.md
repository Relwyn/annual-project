Vous pouvez lancer le conteneur vegeta avec la commande 

```
docker-compose -f docker-compose.perf.yml up -d
```
(Pour lancer tous les conteneurs d'un coup)

```
docker-compose -f docker-compose.perf.yml -f docker-compose.yml up -d
```