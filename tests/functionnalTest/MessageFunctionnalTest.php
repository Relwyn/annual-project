<?php

namespace App\Tests\Repository;

use DateInterval;
use DateTime;
use App\Entity\Message;
use App\Entity\User;
use App\Repository\MessageRepository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class MessageFunctionnalTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    private $client;
    private $user;
    private $friendUser;
    private $dateFirstMessage = "2020-07-10 01:00:00";

    protected function setUp(): void
    {
        $this->client = static::createClient([], [
            'PHP_AUTH_USER' => 'test@test.com',
            'PHP_AUTH_PW'   => 'test',
        ]);

        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->user = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['email' => "test@test.com"])
        ;

        $this->friendUser = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['email' => "friend@gmail.com"])
        ;
    }

    public function testSendingMessage()
    {

        $this->client->request('POST', '/messages/new/'.$this->user->getId(), [
            "message" =>["content"=> "Some message."]
        ]);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
