<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\Tag;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Message;

class TestFixture extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        // Test user
        $user = (new User())
                ->setLastname("test")
                ->setFirstname("tester")
                ->setEmail("test@test.com")
                ->setImageFilename('/build/images/users/melissa.png')
                ->setBackgroundImageFileName('/build/images/background/plage.jpg')
                ->setPlainPassword('test')
                ->setGender($faker->randomElement(User::GENDER))
            ;
        $manager->persist($user);

        // Friend user
        $friendUser = (new User())
            ->setId(1)
            ->setLastname('Friend')
            ->setFirstname('User')
            ->setEmail('friend@gmail.com')
            ->setPlainPassword('string')
            ->setImageFilename('/build/images/users/anis.png')
            ->setBackgroundImageFileName('/build/images/background/plage.jpg')
            ->setGender($faker->randomElement(User::GENDER))
            ->setBirthday(new DateTime($faker->date($format = 'Y-m-d', $max = 'now')));
        ;

        $user->addFriend($friendUser);
        $friendUser->addFriend($user);

        $manager->persist($friendUser);

        $commentForPost = (new Comment())
            ->setContent("testing comment for post")
            ->setUserId($user);
        ;
        $manager->persist($commentForPost);

        $tag = (new Tag())
            ->setName("tagTest")
            ->setLabel("testLabel")
        ;

        $post = (new Post())
            ->setTitle("Post test")
            ->setContent("Post test content")
            ->setUserId($user)
            ->addComment($commentForPost)
            ->addTag($tag)
        ;

        $message = (new Message())
            ->setSenderId($user)
            ->setReceiverId($friendUser)
            ->setCreatedAt(new DateTime("2020-07-10 01:00:00"))
            ->setContent("First message")
        ;

        $manager->persist($tag);
        $manager->persist($post);
        $manager->persist($message);

//        $article = (new Article())
//            ->setCreatedAt(DateTime::createFromFormat("d/m/Y H:i:s", "19/10/2016 14:48:21"))
//            ->setCreatedBy($user)
//            ->setName("test")
//            ->addTag($tag)
//        ;

        $comment = (new Comment())
            ->setContent("testing comment")
            ->setUserId($user)
            ->setPostId($post)
        ;

        
        $manager->persist($comment);
//        $manager->persist($article);
        $manager->persist($tag);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['tests'];
    }
}
