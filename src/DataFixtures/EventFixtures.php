<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EventFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        $users = $manager->getRepository(User::class)->findAll();

        for ($i = 1; $i < 10; $i++) {
            $start = $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now');
            $end = $faker->dateTimeBetween($startDate = $start, $endDate = '+5 days');
            $event = (new Event())
                ->setName($faker->realText(15))
                ->setDescription($faker->realText(255))
                ->setCreator($faker->randomElement($users))
                ->setStartDate($start)
                ->setEndDate($end)
                ->setImageEvent("/build/images/event/event".$i.".jpg")
                ;

            for ($j = 0; $j < rand(2, 10); $j++) {
                $event->addAttender($faker->randomElement($users));
            }

            $manager->persist($event);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }

    public static function getGroups(): array
    {
        return ['fixtures'];
    }
}
