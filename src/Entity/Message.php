<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{

    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="messages")
     * @MaxDepth(2)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"default"})
     */
    private $senderId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="messages")
     * @ORM\JoinColumn(nullable=true)
     * @MaxDepth(3)
     * @Groups({"default"})
     */
    private $receiverId;

    /**
     * @ORM\Column(type="text")
     * @Groups({"default"})
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Groupe", inversedBy="messages")
     */
    private $groupeId;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSenderId(): ?User
    {
        return $this->senderId;
    }

    public function setSenderId(?User $senderId): self
    {
        $this->senderId = $senderId;

        return $this;
    }

    public function getReceiverId(): ?User
    {
        return $this->receiverId;
    }

    public function setReceiverId(?User $receiverId): self
    {
        $this->receiverId = $receiverId;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getGroupeId(): ?Groupe
    {
        return $this->groupeId;
    }

    public function setGroupeId(?Groupe $groupeId): self
    {
        $this->groupeId = $groupeId;

        return $this;
    }

}
