Vous devez créer un fichier .env.performance à la racine. Ce fichier doit contenir : 

```
BLACKFIRE_CLIENT_ID=
BLACKFIRE_CLIENT_TOKEN=

BLACKFIRE_SERVER_ID=
BLACKFIRE_SERVER_TOKEN=
```
Vous pouvez alors executez cette commande pour lancer la sonde blackfire dans le projet symfony

```
docker-compose exec php sh

./blackfire-init.sh
```