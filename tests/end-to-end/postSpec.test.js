// Maximum time for each test
jest.setTimeout(20000);

const puppeteer = require('puppeteer');

let testingUser = {
  email: 'arnaud@gmail.com',
  password: "string",
};

const url = "localhost:7080";

let browser;
let page;

describe('[end-to-end] Post',  () => {

    beforeAll(async () => {
        try {
            browser = await puppeteer.launch({
            headless: false,
            defaultViewport: { width: 1920, height: 1050 }
            });

            page = await browser.newPage();
            await page.authenticate({'username':testingUser.email, 'password': testingUser.password});

            await page.goto(url);

            // await page.evaluate((testingUser) => {
            //
            //     // let testingUser = {
            //     //     email: 'arnaud@gmail.com',
            //     //     password: "string",
            //     // };
            //
            //     let _doc = document;
            //     const _form = _doc.getElementById('login').elements;
            //     _form['inputEmail'].value = testingUser.email;
            //     _form['inputPassword'].value = testingUser.password;
            //
            //     _form['submitLogin'].click();
            //
            // }, testingUser);

            await page.waitFor(2000);

        } catch(error) {
            console.log("beforeAll :", error);
        }
    }),

    test('Post creation', async () => {

        expect.assertions(1);
        try {

            let expectedText = "Post de test #heyhey";

            await page.evaluate((expectedText) => {
                  
                let _doc = document;
                const _form = _doc.getElementById('createPostForm').elements;
                _form['post_content'].value = expectedText;
                _form['sendPost'].click();
    
            },expectedText);

            await page.waitFor(2000);

            let postContent = await page.evaluate((testingUser) => {
                  
                let _doc = document;
                const content = _doc.querySelector('#content .postContent').innerText;
                return content;
    
            }, testingUser);

            expect(expectedText).toBe(postContent);

        } catch (error) {
            console.log("Inscription error: ", error);
        }
       
    });

    afterAll(async () => {
        browser.close();
    });
});


function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }   
