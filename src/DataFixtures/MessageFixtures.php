<?php

namespace App\DataFixtures;


use App\Entity\Groupe;
use App\Entity\Message;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MessageFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        $users = $manager->getRepository(User::class)->findAll();
        $groupes = $manager->getRepository(Groupe::class)->findAll();

        // Create message
//        for ($i = 0; $i < 50; $i++) {
//            $message = (new Message())
//                ->setContent($faker->sentence(5))
//                ->setSenderId($faker->randomElement($users))
//                ->setReceiverId($faker->randomElement($users))
//                ->setDate($faker->dateTime)
//            ;
//
//            $manager->persist($message);
//        }

        // Create group messages
        for ($i = 0; $i < 50; $i++) {
            $currentGroup = $faker->randomElement($groupes);
            $currentUser = $faker->randomElement($users);

            $currentGroup->addUser($currentUser);

            $message = (new Message())
                ->setContent($faker->sentence(5))
                ->setSenderId($currentUser)
                ->setGroupeId($currentGroup)
            ;

            $manager->persist($message);
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(UserFixtures::class,GroupeFixtures::class);
    }
}
