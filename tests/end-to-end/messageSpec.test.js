// Maximum time for each test
jest.setTimeout(20000);

const puppeteer = require('puppeteer');

let testingUser = {
  email: 'test@test.com',
  password: "test",
};

let friendUser = {
    lastname : 'Friend',
    firstname : 'User',
};

const hostname = "http://localhost:7080";
const loginUrl = `${hostname}/login`;
const messageUrl = `${hostname}/messages/`;

let browser;
let page;

describe('[end-to-end] Message',  () => {

    beforeAll(async () => {
        try {
            browser = await puppeteer.launch({
            headless: false,
            defaultViewport: { width: 1920, height: 1050 }
            });

            page = await browser.newPage();
            await page.authenticate({'username':testingUser.email, 'password': testingUser.password});

            // await page.goto(loginUrl);

            // await page.evaluate((testingUser) => {
            //
            //     let _doc = document;
            //     const _form = _doc.getElementById('login').elements;
            //     _form['inputEmail'].value = testingUser.email;
            //     _form['inputPassword'].value = testingUser.password;
            //
            //     _form['submitLogin'].click();
            //
            // }, testingUser);


            await page.waitFor(3000);

            await page.goto(messageUrl);

            await page.waitFor(2000);

            // add friend then join discussion
            await page.evaluate((friendUser) => {

                let _doc = document;
                const _friendList = _doc.getElementById('friendListTable');
                messageLink = _friendList.querySelector(`#${friendUser.firstname}_${friendUser.lastname} .joinDiscussion`);
                messageLink.click();
    
            }, friendUser);

            await page.waitFor(2000);

        } catch(error) {
            console.log("beforeAll :", error);
        }
    }),

    test('Message creation', async () => {

        expect.assertions(1);
        try {

            let expectedMessage = "Salut";

            await page.evaluate((expectedMessage) => {
                  
                let _doc = document;
                _doc.getElementById('message_content').value = expectedMessage;
                _doc.getElementById('message_Envoyer').click();
    
            },expectedMessage);

            await page.waitFor(5000);

            let messageContent = await page.evaluate(() => {
                  
                let _doc = document;
                const content = _doc.querySelector('#messages .sender .content').innerText;
                return content;
    
            });
            // console.log("messageContent = ", messageContent);
            // console.log("expected Message = ", expectedMessage);
            expect(messageContent).toBe(expectedMessage);

        } catch (error) {
            console.log("Inscription error: ", error);
        }
       
    });

    afterAll(async () => {
        browser.close();
    });
});   
