// Maximum time for each test
jest.setTimeout(20000);

const puppeteer = require('puppeteer');

let newUser = {
  email: `inscriptionTest${Math.random()*10}@gmail.com`,
  firstname : "name",
  lastname : "lastname",
  birthday: { day: "12", month: "1", year: "2015"},
  gender : "m",
  image : "test",
  password: "string",
};

let testingUser = {
    email: 'test@test.com',
    password: "test",
};

const url = "localhost:7080/register";

let browser;
let page;

describe('[end-to-end] Inscription',  () => {

    beforeAll(async () => {
        try {
            browser = await puppeteer.launch({
                headless: false,
                defaultViewport: { width: 1920, height: 1050 }
            });

            page = await browser.newPage();
            await page.authenticate({'username':testingUser.email, 'password': testingUser.password});

            await page.goto(url);
            await page.waitFor(2000);

        } catch(error) {
            console.log("beforeAll :", error);
        }
    });

    test('Create user', async () => {

        expect.assertions(1);
        try {
            await page.evaluate((newUser) => {

                let _doc = document;
                const _form = _doc.getElementById('registration').elements;
                _form['registration_form_lastname'].value = newUser.lastname;
                _form['registration_form_firstname'].value = newUser.firstname;
                _form['registration_form_email'].value = newUser.email;
                _form['registration_form_birthday_day'].value = newUser.birthday.day;
                _form['registration_form_birthday_month'].value = newUser.birthday.month;
                _form['registration_form_birthday_year'].value = newUser.birthday.year;
                _form['registration_form_gender'].value = newUser.gender;
                _form['registration_form_plainPassword_first'].value = newUser.password;
                _form['registration_form_plainPassword_second'].value = newUser.password;
                _form['registration_form_agreeTerms'].checked = true;
    
                _doc.getElementById("registration_form_Inscription").click();
    
            }, newUser);
    
            const [response] = await Promise.all([
                page.waitForNavigation(), // This will set the promise to wait for navigation events
                page.click('#registration_form_Inscription')
            ]);
    
            // console.log(response);
            // console.log(response._url.split('/'));
    
            var url = response._url.split('/');
            console.log(url);
    
            expect(url[4].length).toBe(0);

            await page.waitFor(2000);

        } catch (error) {
            console.log("Inscription error: ", error)
        }
       
    });

    afterAll(async () => {
        browser.close();
    });
});
