<?php

namespace App\Controller\Front;

use App\Entity\Event;
use App\Entity\Groupe;
use App\Entity\User;
use App\Form\EventType;
use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/event")
 */
class EventController extends AbstractController
{
    /**
     * @Route("/", name="event_index", methods={"GET"})
     */
    public function index(EventRepository $eventRepository): Response
    {
        return $this->render('front/event/list.html.twig', [
            'events' => $eventRepository->findAll(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/new", name="event_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $event = new Event();
        $event->setCreator($this->getUser());

        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $entityManager = $this->getDoctrine()->getManager();

            $imageFile = $form->get('imageEvent')->getData();
            $createDiscussion = $request->request->get('discussion');

            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = '/event/' . $safeFilename . '-' . uniqid() . '.' . $imageFile->guessExtension();

                try {
                    $imageFile->move(
                        $this->getParameter('images_directory') . '/event/' ,
                        $newFilename
                    );
                } catch (FileException $e) {
                    dump($e);
                }

                $event->setImageEvent('/build/images'.$newFilename);
            }

            if($createDiscussion === "on"){
                $groupe = new Groupe();
                $groupe->addUser($user);
                $groupe->setSubject($event->getName());
                $event->setGroupe($groupe);
            }

            $event->addAttender($user);

            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('event_index');
        }

        return $this->render('front/event/new.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="event_show", methods={"GET"})
     */
    public function show(Event $event): Response
    {
        $currentUser = $this->getUser();
        $isInGroupe = false;
        $isInEvent = false;

        $attenders = $event->getAttenders();

        foreach ($attenders as $user) {
            if($user->getId() === $currentUser->getId())
                $isInEvent = true;
        }


        // If user in event and there is a group
        if( ($isInEvent || $currentUser->getId() == $event->getCreator()->getId()) && $event->getGroupe() ){

            $groupes = $currentUser->getGroupes();

            foreach ($groupes as $group){
                if($group->getId() == $event->getGroupe()->getId())
                    $isInGroupe = true;
            }
        }

        return $this->render('front/event/show.html.twig', [
            'event' => $event,
            'currentUser' => $currentUser,
            'isInGroupe' => $isInGroupe,
            'isInEvent' => $isInEvent
        ]);
    }

    /**
     * @Route("/{id}/edit", name="event_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Event $event): Response
    {
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('event_index');
        }

        return $this->render('front/event/edit.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/attend/{id}", name="event_attend", methods={"GET"})
     */
    public function attend(Request $request, Event $event, MessageBusInterface $bus): Response
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $event->addAttender($user);
        $entityManager->persist($event);
        $entityManager->flush();

        $update = new Update("event/newAttender/".$event->getCreator()->getId(),
            json_encode(['message' => $user->getSlug().' s\'est inscrit à votre évènement ('.$event->getName().') !',
            'url' => 'event/'.$event->getId()])
        );
        $bus->dispatch($update);

        return $this->redirectToRoute('event_show', [
            'id' => $event->getId()
        ]);
    }

    /**
     * @Route("/desattend/{id}", name="event_desattend", methods={"GET"})
     */
    public function desattend(Request $request, Event $event): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $event->removeAttender($this->getUser());
        $entityManager->persist($event);
        $entityManager->flush();

        return $this->redirectToRoute('event_show', [
            'id' => $event->getId()
        ]);
    }

    /**
     * @Route("/desattend/{id}/{userId}", name="event_desattend_user", methods={"GET"})
     */
    public function desattendUser(Request $request, Event $event, int $userId): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($userId);
        $event->removeAttender($user);
        $entityManager->persist($event);
        $entityManager->flush();

        return $this->redirectToRoute('event_show', [
            'id' => $event->getId()
        ]);
    }

    /**
     * @Route("/{id}", name="event_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Event $event): Response
    {
        if ($this->isCsrfTokenValid('delete'.$event->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($event);
            $entityManager->flush();
        }

        return $this->redirectToRoute('event_index');
    }
}
