<?php

namespace App\Controller\Front;

use App\Entity\Comment;
use App\Entity\Post;
use App\Form\CommentType;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/post")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="post_index", methods={"GET"})
     */
    public function index(PostRepository $postRepository): Response
    {
        return $this->render('front/post/index.html.twig', [
            'posts' => $postRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="post_new", methods={"GET","POST"})
     */
    public function new(Request $request, MessageBusInterface $bus): Response
    {
        $post = new Post();
        $user = $this->getUser();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->addPost($post);

            $entityManager->persist($post);
            $entityManager->flush();

            $update = new Update("http://localhost:7080/ping", "[]");
            $bus->dispatch($update);

            return $this->redirectToRoute('post_index');
        }

        return $this->render('front/post/new.html.twig', [
            'user' => $user,
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="post_show", methods={"GET","POST"})
     * @param Request $request
     * @param Post $post
     * @return Response
     */
    public function show(Request $request, Post $post): Response
    {
        $user = $this->getUser();
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $comment->setPostId($post);
            $comment->setUserId($user);

            $entityManager->persist($comment);
            $entityManager->flush();

//            return $this->redirectToRoute('post_show', [
//                'id' => $post->getId(),
//            ]);
        }
        return $this->render('front/post/show.html.twig', [
            'post' => $post,
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/comment/new", name="post_comment_new", methods={"GET","POST"})
     * @param Request $request
     * @param Post $post
     * @param Comment $comment
     * @return Response
     */
    public function newComment(Request $request, Post $post, Comment $comment): Response
    {
        $comment = new Comment();
        $user = $this->getUser();

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $comment->setPostId($post);
            $comment->setUserId($user);

            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('front/post/show.html.twig', [
                'post' => $post,
            ]);
        }

        return $this->render('front/post/new.html.twig', [
            'user' => $user,
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("{commentId}/{id}/comment/delete", name="post_comment_delete", methods={"DELETE"})
     * @param Request $request
     * @param Post $post
     * @param Comment $comment
     * @return Response
     */
    public function deleteComment(Request $request, Post $post, int $commentId ): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $comment = $entityManager->getRepository(Comment::class)->find($commentId);
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('post_show', [
            'id' => $post->getId(),
        ]);
    }

//    /**
//     * @Route("/{id}/edit", name="post_edit", methods={"GET","POST"})
//     */
//    public function edit(Request $request, Post $post): Response
//    {
//        $form = $this->createForm(PostType::class, $post);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $this->getDoctrine()->getManager()->flush();
//
//            return $this->redirectToRoute('post_index');
//        }
//
//        return $this->render('front/post/edit.html.twig', [
//            'post' => $post,
//            'form' => $form->createView(),
//        ]);
//    }

    /**
     * @Route("/{id}", name="post_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Post $post): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirectToRoute('default_index');
    }
}
