<?php

namespace App\Tests\Repository;

use DateInterval;
use DateTime;
use App\Entity\Message;
use App\Entity\User;
use App\Repository\MessageRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MessageUnitTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    private $user;
    private $friendUser;
    private $dateFirstMessage = "2020-07-10 01:00:00";

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->user = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['email' => "test@test.com"])
        ;

        $this->friendUser = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['email' => "friend@gmail.com"])
        ;
    }

    public function testGetMessagesAfterDate()
    {
        $dateMessage = new DateTime($this->dateFirstMessage);
        // minus 30sec
        $dateMessage->sub(new DateInterval('PT30S'));

        $message = $this->entityManager
            ->getRepository(Message::class)
            ->getMessages($this->user->getId(), $this->friendUser->getId(), $dateMessage )
        ;

        $this->assertFalse(empty($message));

    }

    public function testSendingMessage()
    {

        $dateMessage = new DateTime($this->dateFirstMessage);
        // minus 30sec
        $dateMessage->sub(new DateInterval('PT30S'));

        $message = $this->entityManager
            ->getRepository(Message::class)
            ->getMessages($this->user->getId(), $this->friendUser->getId(), $dateMessage )
        ;

        $this->assertFalse(empty($message));

    }


    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
