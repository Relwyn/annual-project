<?php

namespace App\Controller\Front;

use App\Entity\Groupe;
use App\Entity\Message;
use App\Entity\User;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use DateTime;
use Doctrine\Common\Annotations\AnnotationReader;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
//use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\View\View;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/messages")
 */
class MessageController extends AbstractController
{
    /**
     * @Route("/", name="message_index", methods={"GET"})
     */
    public function index(MessageRepository $messageRepository): Response
    {
        $userFriends = $this->getUser()->getFriends();
        $groupsOfUser = $this->getUser()->getGroupes();
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        $groupes = $this->getDoctrine()->getRepository(Groupe::class)->findAll();

        return $this->render('front/message/discussionList.html.twig', [
            'userFriends' => $userFriends,
            'groupsOfUser' => $groupsOfUser,
            'users' => $users,
            'groupes' => $groupes,
        ]);
    }

    /**
     * @Route("/discussion/{id}", name="message_discussion", methods={"GET","POST"})
     */
    public function discussion(Request $request, User $receiver): Response
    {
        $newMessage = new Message();
        $user = $this->getUser();
        $messages = $this->getDoctrine()->getRepository(Message::class)->getMessages($receiver->getId(), $user->getId());

        $form = $this->createForm(MessageType::class, $newMessage);
        $form->handleRequest($request);

        return $this->render('front/message/discussion.html.twig', [
            'user' => $user,
            'receiver' => $receiver,
            'messages' => $messages,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/discussion/group/{id}", name="message_discussion_group", methods={"GET","POST"})
     */
    public function discussionGroup(Request $request, Groupe $groupe): Response
    {
        $newMessage = new Message();
        $user = $this->getUser();
        $messages = $this->getDoctrine()->getRepository(Message::class)->getMessagesGroupe($groupe->getId());

        $form = $this->createForm(MessageType::class, $newMessage);
        $form->handleRequest($request);

        return $this->render('front/message/discussion.html.twig', [
            'user' => $user,
            'groupe' => $groupe,
            'messages' => $messages,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="message_discussion_new", methods={"GET","POST"})
     */
    public function newMessage(Request $request, User $receiver, MessageBusInterface $bus, LoggerInterface $logger): Response
    {
        $logger->error('I just got the logger');
        $newMessage = new Message();
        $user = $this->getUser();

        $form = $this->createForm(MessageType::class, $newMessage);
        $form->handleRequest($request);

        $entityManager = $this->getDoctrine()->getManager();
        $user->addMessage($newMessage);
        $newMessage->setReceiverId($receiver);

        $entityManager->persist($newMessage);
        $entityManager->flush();

        $update = new Update("message/newMessage/".$receiver->getId(),
            json_encode(['message' => '('.$user->getSlug().') '.$newMessage->getContent(),
            'url' => 'messages/discussion/'.$user->getId()])
        );
        $bus->dispatch($update);

        return $this->json(['200', 'sended']);
    }

    /**
     * @Route("/new/group/{id}", name="message_group_new", methods={"GET","POST"})
     */
    public function newMessageGroupe(Request $request, Groupe $groupe, MessageBusInterface $bus): Response
    {
        $newMessage = new Message();
        $user = $this->getUser();

        $form = $this->createForm(MessageType::class, $newMessage);
        $form->handleRequest($request);

        $entityManager = $this->getDoctrine()->getManager();
        $user->addMessage($newMessage);
        $newMessage->setGroupeId($groupe);

        $entityManager->persist($newMessage);
        $entityManager->flush();

        $attenders = $groupe->getUsers();

        foreach ($attenders as $attender) {
            dump($attender->getId());
            if($attender->getId() != $user->getId()){
                $update = new Update("message/newMessage/".$attender->getId(),
                    json_encode(['message' => '( '.$groupe->getSubject().' '.$user->getSlug().') '.$newMessage->getContent(),
                        'url' => 'messages/discussion/group/'.$groupe->getId()])
                );
                $bus->dispatch($update);
            }
        }

        return $this->json(['200', 'sended']);
    }

    /**
     * @Route("/get/{id}", name="message_discussion_get", methods={"POST"})
     */
    public function getMessages(Request $request, User $receiver): Response
    {
        $newMessage = new Message();
        $user = $this->getUser();
        $string = $request->request->all();
        $string = $string['lastDate'];
        $date = new DateTime($string);
        dump($date);
        $messages = $this->getDoctrine()->getRepository(Message::class)->getMessages($receiver->getId(), $user->getId(), $date);

        $form = $this->createForm(MessageType::class, $newMessage);
        $form->handleRequest($request);

        for ($i = 0; $i < sizeof($messages); $i++) {
            $receiver = $this->getDoctrine()->getRepository(User::class)->find($messages[$i]['receiver_id_id']);
            $sender = $this->getDoctrine()->getRepository(User::class)->find($messages[$i]['sender_id_id']);
            $messages[$i]['receiverId'] = [];
            $messages[$i]['senderId'] = [];

            $messages[$i]['receiverId']['lastname'] = $receiver->getLastname();
            $messages[$i]['senderId']['lastname'] = $sender->getLastname();

            $messages[$i]['receiverId']['firstname'] = $receiver->getFirstname();
            $messages[$i]['senderId']['firstname'] = $sender->getFirstname();

            $messages[$i]['receiverId']['imageFileName'] = $receiver->getImageFileName();
            $messages[$i]['senderId']['imageFileName'] = $sender->getImageFileName();
        }

        dump($messages);
        if (!empty($messages)) {
            return new JsonResponse($messages);
        } else {
            return new JsonResponse(["empty" => true]);
        }
    }

    /**
     * @Route("/get/group/{id}", name="message_discussion_get_groupes", methods={"POST"})
     */
    public function getMessagesGroupe(Request $request, Groupe $groupe): Response
    {
        $newMessage = new Message();
        $string = $request->request->all();
        $string = $string['lastDate'];
        $date = new DateTime($string);
        dump($date);
        $messages = $this->getDoctrine()->getRepository(Message::class)->getMessagesGroupe($groupe->getId(), $date);

        $form = $this->createForm(MessageType::class, $newMessage);
        $form->handleRequest($request);

        for ($i = 0; $i < sizeof($messages); $i++) {

            $sender = $this->getDoctrine()->getRepository(User::class)->find($messages[$i]['sender_id_id']);
            $messages[$i]['senderId'] = [];
            $messages[$i]['senderId']['lastname'] = $sender->getLastname();
            $messages[$i]['senderId']['firstname'] = $sender->getFirstname();
        }

        dump($messages);
        if (!empty($messages)) {
            return new JsonResponse($messages);
        } else {
            return new JsonResponse(["empty" => true]);
        }
    }

}