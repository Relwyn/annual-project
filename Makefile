install:
	docker-compose exec php composer install && docker-compose exec php yarn install

test_unit:
	docker-compose exec php bin/phpunit tests/unitTest &&

test_functionnal:
    docker-compose exec php bin/phpunit tests/functionnalTest

test_bdd:
	docker-compose exec php yarn run test

test: test_unit test_functionnal test_bdd
