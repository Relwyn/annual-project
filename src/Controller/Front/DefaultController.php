<?php

namespace App\Controller\Front;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\Tag;
use App\Form\CommentType;
use App\Form\PostType;
use App\Service\NotificationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package App\Controller\Front
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default_index", methods={"GET","POST"})
     */
    public function index(Request $request, MessageBusInterface $bus, MailerInterface $mailer, NotificationService $notification): Response
    {
        $comment = new Comment();
        $post = new Post();
        $user = $this->getUser();
        $requestData = $request->request->all();

        if (isset($requestData["postId"])) {
            $form = $this->createForm(CommentType::class, $comment);
            $form->handleRequest($request);
            // Check if the form is valid
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $post = $this->getDoctrine()->getRepository(Post::class)->find($requestData["postId"]);
                $comment->setPostId($post);
                $comment->setUserId($user);
                $entityManager->persist($comment);
                $entityManager->flush();

                return $this->redirectToRoute('post_show', [
                    'id' => $post->getId(),
                ]);
            }
        } else {
            $form = $this->createForm(PostType::class, $post);
            $form->handleRequest($request);
            // Check if the form is valid
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                // Get tags and save them
                $postContent = $post->getContent();
                preg_match_all("~#[a-z]*( |$)~", $postContent, $tags);
                $tags = $tags[0];

                for($j = 0; $j < sizeof($tags); $j++){
                    $tagName = $tags[$j];
                    $tagName = substr($tagName, 1);
                    $tag = new Tag();
                    $tag->setName($tagName);
                    $tag->addPostId($post);
                    $entityManager->persist($tag);
                    $post->addTag($tag);
                }

                $post->setUserId($user);
                $entityManager->persist($post);
                $entityManager->flush();

                // Send notification to friends
                $friends = $user->getFriends();
                foreach ($friends as $friend) {
                    $id = $friend->getId();
                    dump("post/newPost/".$id);
                    $update = new Update("post/newPost/".$id,
                        json_encode(['message' => $user->getSlug().' a créer un nouveau post !'])
                    );
                    $bus->dispatch($update);
                }

                //return $this->redirectToRoute('default_index');
            }
        }
        $posts = $this->getDoctrine()->getRepository(Post::class)->findTenPost();

        // create postFrom
        $postForm = $this->createForm(PostType::class, $post);
        // create comment Form
        $data = [];
        for ($i = 0; $i < sizeof($posts); $i++) {
            $data[$i] = [];
            $data[$i]["post"] = $posts[$i];
            $form = $this->createForm(CommentType::class, $comment);
            $data[$i]["form"] = $form->createView();
        }

        return $this->render('front/user/show.html.twig', [
            'data' => $data,
            'postForm' => $postForm->createView(),
            'user' => $user
        ]);
    }
}
